# Self-update

## Including self-update in your program

In your program, Add the following code, 

	package main

	import (
		"log"
		"github.com/ishanjain28/go-selfupdate/selfupdate"
	)

	func main() {
		version := "1.1"
		updater := &selfupdate.Updater{
			CurrentVersion: version,                    // Using Semantic Versioning
			ApiURL:         "<api address>", 			// Api Url where JSON is stored
			BinURL:         "<binary address>", 		// url where binaries are stored
			CmdName:        "self_update",              // The name of program
			Dir:            "update-tmp",               // folder name for temporary local storage
			ForceCheck:     true,
		}

		if updater != nil {
			err := updater.BackgroundRun()
			if err != nil {
				log.Fatal(err.Error())
			}
		}
	}

* The JSON files containing the name of latest version and the sha256 hash of binary is located at API Address
*  go-selfupdate downloads the binary from Binary URL(`BinURL`)
*  This program will check for the latest version by making a call to API, If a newer version of binary is available, it'll download the latest version and update the current binary. When the binary is restarted, The newer version of binary will be executed. 

## Creating an update of binary

go-selfupdate comes with a CLI tool that can be used to release new binary. 

When you want to release a newer version of binary, Do the following. 

* Put tag of newer version in`version` variable in aforementioned source code. When the binary is built, The `updater.CurrentVersion` field will contain correct version tag. 
* Build the binary using `go build` or whatever command you use to build your project. 
* Run, `go-selfupdate <name of binary> <newer version tag>`
* Last step will create a public folder, Upload the contents of this folder to a server. 


Before you build the binary, Be sure to set ApiURL and BinURL correctly and once you have ran `go-selfupdate` command, upload the contents of newly created `public` folder to your server. 


 


package main

import (
	"log"

	"github.com/ishanjain28/go-selfupdate/selfupdate"
)

func main() {

	version := "1.0"
	updater := &selfupdate.Updater{
		CurrentVersion: version,                    // Using Semantic Versioning
		ApiURL:         "https://dl.ishanjain.me/", // Api Url where JSON is stored
		BinURL:         "https://dl.ishanjain.me/", // url where binaries are stored
		CmdName:        "self_update",              // The name of program
		Dir:            "update-tmp",               // folder name for temporary local storage
		ForceCheck:     true,
	}

	log.Println("Version:", updater.CurrentVersion)

	if updater != nil {
		err := updater.BackgroundRun()
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	log.Printf("Update complete: %s %s %x", updater.CurrentVersion, updater.Info.Version, updater.Info.Sha256)
	for {
	}
}
